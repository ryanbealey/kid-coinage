// Information
const info = {
    "corporation": "Unpackage, Inc.",
    "software": {
        "name": "Kid Coinage",
        "version": "0.0.1",
        "description": "Kid Coinage helps parents moderate and monitor their child's financial success."
    },
};

// Depedencies
let bcrypt = require("bcrypt");
let chalk = require("chalk");
let express = require("express");
let exphbs = require("express-handlebars");
let expfu = require("express-fileupload");
let logger = require("morgan");
let moment = require("moment");
let mongoose = require("mongoose");
let parser = require("body-parser");
let path = require("path");
let stripe = require("./config/stripe.js");

// MongoDB
let db = require("./models");
let PORT = process.env.PORT || 3000;

// ExpressJS 
let app = express();
let URI = process.env.MONGODB_URI || "mongodb://localhost/kidcoinage";

// MonogDB Initialize
mongoose.Promise = Promise;
mongoose.connect(URI);

// Middleware
app.use(express.static("public"));
app.engine("handlebars", exphbs({ defaultLayout: "main" }));
app.set("view engine", "handlebars");
app.use(expfu());
app.use(logger("dev"));
app.use(parser.json());
app.use(parser.urlencoded({ extended: true }));

// Public Views
require("./routes/app")(app);

// Launch
app.listen(PORT, function(err, res){
    if (err){
        console.log("An error is afoot!");
    } else {
        console.log(chalk.yellow(`${info.corporation}\n${info.software.name} || v${info.software.version}`));
    };
});