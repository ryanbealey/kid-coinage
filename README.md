# Kid Coinage
## Debit cards for kids, peace of mind for parents.

### Overview
Kid Coinage is a new and emerging parental finance tool that helps parents and guardians monitor and restrict their children's transactions both online and in physical commercial locations.

### For Parents
Parents are able to issue, activate, disable or cancel their child's debit card, monitor transactions, moderate where and when kids can use their debit card and even report and manage fraud or dispute cases on charges your child did not create.

1. **Monitor and track** your child's financial transactions.
2. **Moderate** when and where your child can use their card.
3. **Add and remove funds** when needed, no funds in the pot? No problem. Deposits are instant, it'll just take a few seconds.
4. **Set an allowance schedule** so that you can deposit funds to your child's account automagically on a recurring basis; either weekly, bi-weekly or monthly.
5. **Report fraud or lost cards easily** through our online portal.
6. Add as many children to your account as you need.

Kid Coinage is only $3.49 per month per cardholder; no contracts or commitments. You can even sign up for a free trial if you'd like to try it out before signing up.

### For Kids
Kids and teens will love Kid Coinage because it gives them the flexibility and freedom of a debit card so that, instead of carrying around the hastle of cash, they have their entire allowance balance at their fingertips.
Kid Coinage helps to mitigate loss of money with card security and the ability to turn off the card with the tap of a button. Additionally, Kid Coinage offers a suite of fraud prevention and reporting tools, so if your card ever gets into the wrong hands, all you have to do is let us know.
It makes you look pretty cool, too. ;)

1. Kids are granted the responsibility of adulthood -- overdraft and card replacement fees are always deducted from the cardholder's balance, never the the parent's bank account.
2. Receive your allowance automagically through Kid Coinage's allowance configuration set by your parents.
3. If you're ever in a pickle, send a request to your folks for your specified amount of money and receive it in as long as it takes for your parents to approve that request.