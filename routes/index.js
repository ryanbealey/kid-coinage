var router = require("express").Router();
var api = require("./api");
var views = require("./app");

router.use("/api", api);
router.use("/", views);

module.exports = router;